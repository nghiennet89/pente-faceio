<!DOCTYPE html>
<html>
<head>
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta name=viewport content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <title>@yield('title')</title>
    <link href={{ asset('/css/style.css') }} rel=stylesheet>
    @if ($_SERVER['SERVER_NAME'] != env('DEV_URL'))
        <link href={{ asset('/static/css/site.css') }} rel=stylesheet>
    @endif
    <link href={{ asset('/css/site.css') }} rel=stylesheet>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
<div id="site"></div>
@if ($_SERVER['SERVER_NAME'] == env('DEV_URL'))
    <script src="http://localhost:8080/site.js"></script>
@else
    <script type=text/javascript src={{ asset('/static/js/manifest.js') }}></script>
    <script type=text/javascript src={{ asset('/static/js/vendor.js') }}></script>
    <script type=text/javascript src={{ asset('/static/js/site.js') }}></script>
@endif
<script type=text/javascript src={{ asset('/js/jquery-1.12.4.js') }}></script>
<script type=text/javascript src={{ asset('/js/site.js') }}></script>
</body>
</html>
