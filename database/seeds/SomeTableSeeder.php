<?php

use App\Model\Admin;
use App\Model\Customer;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class SomeTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('admin')->truncate();
        Admin::firstOrCreate([
            'email'    => 'nghiennet89@gmail.com',
            'username' => 'admin',
            'password' => Hash::make('admin123@'),
        ]);
        Customer::firstOrCreate([
            'email'                   => 'toong@pente.com',
            'username'                => 'toong',
            'password'                => Hash::make('toong123@'),
            'pente_secret_key'        => 'LmNvbS52bjpUb29uZzEyNk1pbmhLa',
            'third_party_auth_type'   => Customer::THIRD_PARTY_AUTH_TYPE['NEXUDUS'],
            'third_party_auth_config' => json_encode([
                'BasicAuth' => 'bmdvYy50cmFuQHRvb25nLmNvbS52bjpUb29uZzEyNk1pbmhLaGFp',
            ]),
        ]);
    }
}
