<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMember extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('member', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id', false, true);
            $table->string('username', 100);
            $table->string('email', 100)->unique();
            $table->string('password');
            $table->longText('member_data_on_third_party');
            $table->longText('aws_image_ids');
            $table->foreign('customer_id')->references('id')->on('customer');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('member');
    }
}
