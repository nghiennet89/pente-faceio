<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('door', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id', false, true);
            $table->integer('ref_id')->nullable();
            $table->string('name', 255)->default('');
            $table->foreign('customer_id')->references('id')->on('customer');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('door');
    }
}
