<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberDoor extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('member_door', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id', false, true);
            $table->integer('door_id', false, true);
            $table->foreign('member_id')->references('id')->on('member');
            $table->foreign('door_id')->references('id')->on('door');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('member_door');
    }
}
