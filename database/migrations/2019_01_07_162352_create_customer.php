<?php

use App\Model\Customer;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomer extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('customer', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 100);
            $table->string('email', 100)->unique();
            $table->string('password');
            $table->integer('third_party_auth_type')->default(Customer::THIRD_PARTY_AUTH_TYPE['NONE']);
            $table->longText('third_party_auth_config');
            $table->string('pente_secret_key', 455)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('customer');
    }
}
