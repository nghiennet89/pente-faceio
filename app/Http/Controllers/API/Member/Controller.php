<?php

namespace App\Http\Controllers\API\Member;

use App\Http\Controllers\API\RESBase;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

class Controller extends RESBase {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, AuthenticatesUsers;
    protected $member;

    public function __construct() {
        parent::__construct();
        $this->member = auth('api-member')->user();
        $this->defaultModelAttribs['member_id'] = $this->member ? $this->member->id : null;
        if (!$this->member) return $this->responseBuilder->fail('Invalid Member', 401);
    }
}
