<?php

namespace App\Http\Controllers\API\Member;

use App\Model\Door;

class DoorController extends Controller {

    public function __construct() {
        $this->modelClass = Door::class;
        $this->availMethods = ['index'];
        return parent::__construct();
    }
}
