<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller as BaseController;
use App\Utils\ResponseBuilder;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

class ApiController extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $responseBuilder;

    public function __construct() {
        $this->responseBuilder = new ResponseBuilder();
    }
}
