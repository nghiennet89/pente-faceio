<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RESBase extends ApiController {
    protected $modelClass          = null;
    protected $defaultModelAttribs = [];
    protected $availMethods        = [
        'index',
        'store',
        'show',
        'update',
        'destroy'];

    protected function index(Request $request) {
        if (!$this->checkMethod('index')) return $this->responseBuilder->fail('Invalid Request', 401);
        $itemPerPage = $request->input('per_page', 200);
        $this->responseBuilder->data = $this->modelClass::query()->where($this->defaultModelAttribs)->paginate($itemPerPage);
        return $this->responseBuilder->build(true);
    }

    protected function checkMethod($methodName) {
        if (!$this->modelClass) return false;
        if (in_array($methodName, $this->availMethods) === false) return false;
        return true;
    }

    protected function create() {
        return $this->responseBuilder->fail('Invalid Request', 401);
    }

    protected function store(Request $request) {
        if (!$this->checkMethod('store')) return $this->responseBuilder->fail('Invalid Request', 401);
        $item = new $this->modelClass();
        $item->fill($request->all());
        if ($item->getAttribute('password')) $item->password = Hash::make($item->password);
        if (sizeof($this->defaultModelAttribs) > 0) foreach ($this->defaultModelAttribs as $key => $value) if (in_array($key, $item->getFillable())) $item->$key = $value;
        try {
            if ($item->save()) return $this->responseBuilder->success('Item added', $item);
            else return $this->responseBuilder->fail('Can not add this item');
        } catch (\Exception $e) {
            return $this->responseBuilder->fail($e->getMessage());
        }
    }

    protected function show($id) {
        if (!$this->checkMethod('show')) return $this->responseBuilder->fail('Invalid Request', 401);
        $item = $this->modelClass::query()->where($this->defaultModelAttribs)->find($id);
        if ($item) return $this->responseBuilder->build(true, null, '', $item);
        else return $this->responseBuilder->build(false, null, 'Invalid item', null);
    }

    protected function edit($id) {
        return $this->responseBuilder->fail('Invalid Request', 401);
    }

    protected function update(Request $request, $id) {
        if (!$this->checkMethod('update')) return $this->responseBuilder->fail('Invalid Request', 401);
        $item = $this->modelClass::query()->where($this->defaultModelAttribs)->find($id);
        if ($item === null) return $this->responseBuilder->fail('Item not found');
        $item->fill($request->all());
        $item->id = $id;
        if (sizeof($this->defaultModelAttribs) > 0) foreach ($this->defaultModelAttribs as $key => $value) if (in_array($key, $item->getFillable())) $item->$key = $value;
        if (in_array('password', $item->getFillable()) && $request->input('password', false)) $item->password = Hash::make($item->password);
        try {
            if ($item->save()) return $this->responseBuilder->success('Item updated', $item);
            else return $this->responseBuilder->fail('Can not update this item');
        } catch (\Exception $e) {
            return $this->responseBuilder->fail($e->getMessage());
        }
    }

    protected function destroy($id) {
        if (!$this->checkMethod('destroy')) return $this->responseBuilder->fail('Invalid Request', 401);
        $item = $this->modelClass::query()->where($this->defaultModelAttribs)->find($id);
        if ($item === null) return $this->responseBuilder->fail('Item not found');
        try {
            if ($item->delete()) return $this->responseBuilder->success('Item deleted');
            return $this->responseBuilder->fail('Can not delete this item');
        } catch (\Exception $e) {
            return $this->responseBuilder->fail($e->getMessage());
        }
    }
}
