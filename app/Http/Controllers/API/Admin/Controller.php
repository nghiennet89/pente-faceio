<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\API\RESBase;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

class Controller extends RESBase {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, AuthenticatesUsers;
    protected $admin;

    public function __construct() {
        parent::__construct();
        $this->admin = auth('api-admin')->user();
        if (!$this->admin) return $this->responseBuilder->fail('Invalid Admin', 401);
    }
}
