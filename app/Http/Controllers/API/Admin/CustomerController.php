<?php

namespace App\Http\Controllers\API\Admin;

use App\Model\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller {

    public function __construct() {
        $this->modelClass = Customer::class;
        return parent::__construct();
    }

    protected function store(Request $request) {
        if (!$this->checkMethod('store')) return $this->responseBuilder->fail('Invalid Request', 401);
        $item = new $this->modelClass();
        $item->fill($request->all());
        if (!is_array($item->third_party_auth_config)) return $this->responseBuilder->fail('Field "third_party_auth_config" is invalid');
        if (!is_string($item->third_party_auth_config)) $item->third_party_auth_config = json_encode($item->third_party_auth_config);
        if ($item->getAttribute('password')) $item->password = Hash::make($item->password);
        if (sizeof($this->defaultModelAttribs) > 0) foreach ($this->defaultModelAttribs as $key => $value) $item->$key = $value;
        try {
            if ($item->save()) return $this->responseBuilder->success('Item added', $item);
            else return $this->responseBuilder->fail('Can not add this item');
        } catch (\Exception $e) {
            return $this->responseBuilder->fail($e->getMessage());
        }
    }

    protected function update(Request $request, $id) {
        if (!$this->checkMethod('update')) return $this->responseBuilder->fail('Invalid Request', 401);
        $item = $this->modelClass::query()->where($this->defaultModelAttribs)->find($id);
        if ($item === null) return $this->responseBuilder->fail('Item not found');
        $item->fill($request->all());
        $item->id = $id;
        if (!is_array($item->third_party_auth_config)) return $this->responseBuilder->fail('Field "third_party_auth_config" is invalid');
        if (!is_string($item->third_party_auth_config)) $item->third_party_auth_config = json_encode($item->third_party_auth_config);
        if ($item->getAttribute('password') && $request->input('password', false)) $item->password = Hash::make($item->password);
        if (sizeof($this->defaultModelAttribs) > 0) foreach ($this->defaultModelAttribs as $key => $value) $item->$key = $value;
        try {
            if ($item->save()) return $this->responseBuilder->success('Item updated', $item);
            else return $this->responseBuilder->fail('Can not update this item');
        } catch (\Exception $e) {
            return $this->responseBuilder->fail($e->getMessage());
        }
    }
}
