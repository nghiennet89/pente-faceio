<?php

namespace App\Http\Controllers\API\Admin;

use App\Model\Customer;
use App\Model\Member;
use App\Model\MemberDoor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class MemberDoorController extends Controller {
    protected $customer;
    protected $member;

    public function __construct() {
        $this->modelClass = MemberDoor::class;
        parent::__construct();

        $customerId = Route::current() ? Route::current()->parameter('customerId') : -1;
        $this->customer = Customer::query()->find($customerId);
        $this->defaultModelAttribs['customer_id'] = $this->customer ? $this->customer->id : null;
        if (!$this->customer) return $this->responseBuilder->fail('Invalid customer');

        $memberId = Route::current() ? Route::current()->parameter('memberId') : -1;
        $this->member = Member::query()->where($this->defaultModelAttribs)->find($memberId);
        $this->defaultModelAttribs['member_id'] = $this->member ? $this->member->id : null;
        if (!$this->member) return $this->responseBuilder->fail('Invalid member');
    }

    protected function index(Request $request) {
        if (!$this->checkMethod('index')) return $this->responseBuilder->fail('Invalid Request', 401);
        $itemPerPage = $request->input('per_page', 200);
        $this->responseBuilder->data = MemberDoor::query()->where($this->defaultModelAttribs)->paginate($itemPerPage);
        return $this->responseBuilder->build(true);
    }
}
