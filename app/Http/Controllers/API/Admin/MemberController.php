<?php

namespace App\Http\Controllers\API\Admin;

use App\Model\Customer;
use App\Model\Member;
use Illuminate\Support\Facades\Route;

class MemberController extends Controller {
    protected $customer;

    public function __construct() {
        $this->modelClass = Member::class;
        parent::__construct();

        $customerId = Route::current() ? Route::current()->parameter('customerId') : -1;
        $this->customer = Customer::query()->find($customerId);
        $this->defaultModelAttribs['customer_id'] = $this->customer ? $this->customer->id : null;
        if (!$this->customer) return $this->responseBuilder->fail('Invalid customer');
    }
}
