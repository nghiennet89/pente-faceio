<?php

namespace App\Http\Controllers\API\Admin;

use App\Model\Customer;
use App\Model\Member;
use App\Model\MemberDoor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class DoorMemberController extends Controller {
    protected $customer;
    protected $door;

    public function __construct() {
        $this->modelClass = MemberDoor::class;
        parent::__construct();

        $customerId = Route::current() ? Route::current()->parameter('customerId') : -1;
        $this->customer = Customer::query()->find($customerId);
        $this->defaultModelAttribs['customer_id'] = $this->customer ? $this->customer->id : null;
        if (!$this->customer) return $this->responseBuilder->fail('Invalid customer');

        $doorId = Route::current() ? Route::current()->parameter('doorId') : -1;
        $this->door = Member::query()->where($this->defaultModelAttribs)->find($doorId);
        $this->defaultModelAttribs['door_id'] = $this->door ? $this->door->id : null;
        if (!$this->door) return $this->responseBuilder->fail('Invalid member');
    }

    protected function index(Request $request) {
        if (!$this->checkMethod('index')) return $this->responseBuilder->fail('Invalid Request', 401);
        $itemPerPage = $request->input('per_page', 200);
        $this->responseBuilder->data = MemberDoor::query()->where($this->defaultModelAttribs)->paginate($itemPerPage);
        return $this->responseBuilder->build(true);
    }
}
