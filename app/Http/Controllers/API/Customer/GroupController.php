<?php

namespace App\Http\Controllers\API\Customer;

use App\Model\Group;

class GroupController extends Controller {
    
    public function __construct() {
        $this->modelClass = Group::class;
        return parent::__construct();
    }
    
    public function getGroupsWithMemberCount() {
        $this->responseBuilder->data = Group::query()->where('customer_id', '=', $this->customer->id)
            ->leftJoin('member_group', 'group.id', 'member_group.group_id')
            ->selectRaw('group.*, COUNT(member_group.id) AS members')
            ->groupBy('group.id')
            ->paginate();
        return $this->responseBuilder->build(true);
    }
}
