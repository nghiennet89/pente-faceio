<?php

namespace App\Http\Controllers\API\Customer;

use App\Http\Controllers\API\RESBase;
use App\Model\Door;
use App\Model\DoorAccessLog;
use App\Utils\SMCS;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;

class Controller extends RESBase {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, AuthenticatesUsers;
    protected $customer;
    
    public function __construct() {
        parent::__construct();
        $this->customer = auth('api-customer')->user();
        $this->defaultModelAttribs['customer_id'] = $this->customer ? $this->customer->id : null;
        if (!$this->customer) return $this->responseBuilder->fail('Invalid Customer', 401);
    }
    
    public function openDoor(Request $request) {
        $doorId = $request->input('door_id', null);
        $penteSecretkey = $request->input('secret_key', '');
        $door = Door::query()->with('customer')->find($doorId);
        if (!$door || !$door->customer || $door->customer->pente_secret_key !== $penteSecretkey) return $this->responseBuilder->fail('Not enough permission');
        $res = SMCS::openDoor([
            'door_id'    => $door->ref_id,
            'secret_key' => $penteSecretkey]);
        if ($res === false) return $this->responseBuilder->fail('Have an error when open door');
        else {
            if (isset($res['success']) && $res['success'] === true) {
                $memberId = $request->input('member_id', null);
                if ($memberId) {
                    $log = new DoorAccessLog();
                    $log->door_id = $door->id;
                    $log->member_id = $memberId;
                    $log->save();
                }
                return $this->responseBuilder->success('Success', $res['message']);
            }
            return $this->responseBuilder->fail(isset($res['message']) ? $res['message'] : 'Door control system fail');
        }
    }
    
    public function customerInfo() {
        return $this->responseBuilder->success('', $this->customer);
    }
}
