<?php

namespace App\Http\Controllers\API\Customer;

use App\Model\Door;
use App\Model\Member;
use App\Model\MemberDoor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class DoorMemberController extends Controller {
    protected $door;

    public function __construct() {
        $this->modelClass = MemberDoor::class;
        parent::__construct();
        $doorId = Route::current() ? Route::current()->parameter('doorId') : -1;
        $this->door = Door::query()->where($this->defaultModelAttribs)->find($doorId);
        if (!$this->door) return $this->responseBuilder->fail('Invalid member');
        $this->defaultModelAttribs['door_id'] = $doorId;
    }

    protected function index(Request $request) {
        if (!$this->checkMethod('index')) return $this->responseBuilder->fail('Invalid Request', 401);
        $itemPerPage = $request->input('per_page', 200);
        $this->responseBuilder->data = MemberDoor::query()->with('member')->where('door_id', '=', $this->defaultModelAttribs['door_id'])->paginate($itemPerPage);
        return $this->responseBuilder->build(true);
    }

    protected function getAllMemberCanBeAdd() {
        $this->responseBuilder->data = Member::query()
            ->where('customer_id', '=', $this->customer->id)
            ->whereRaw('id NOT IN (select member_id from member_door where door_id = ' . $this->defaultModelAttribs['door_id'] . ')')
            ->get();
        return $this->responseBuilder->build(true);
    }

    protected function deleteMemberDoor($id) {
        $memberDoorId = Route::current() ? Route::current()->parameter('memberDoorId') : -1;
        if (!$this->checkMethod('destroy')) return $this->responseBuilder->fail('Invalid Request', 401);
        $item = MemberDoor::query()->find($memberDoorId);
        if ($item === null) return $this->responseBuilder->fail('Item not found');
        try {
            if ($item->delete()) return $this->responseBuilder->success('Item deleted');
            return $this->responseBuilder->fail('Can not delete this item');
        } catch (\Exception $e) {
            return $this->responseBuilder->fail($e->getMessage());
        }
    }
}
