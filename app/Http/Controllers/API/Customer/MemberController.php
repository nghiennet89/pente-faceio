<?php

namespace App\Http\Controllers\API\Customer;

use App\Model\Door;
use App\Model\Member;
use App\Model\MemberDoor;
use App\Utils\SystemUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class MemberController extends Controller {
    
    public function __construct() {
        $this->modelClass = Member::class;
        return parent::__construct();
    }
    
    public function checkUserOnThirdparty(Request $request) {
        $thirdparty = $this->customer->getThirdparty();
        $res = $thirdparty->canAddUser($request->all());
        if ($res === false || !isset($res['status']) || $res['status'] === false) return $this->responseBuilder->fail('Invalid user');
        return $this->responseBuilder->success('Member available', json_encode($res['data']));
    }
    
    public function checkMemberOnThirdparty(Request $request) {
        $memberId = $request->input('member_id', null);
        $member = Member::query()->find($memberId);
        $thirdparty = $this->customer->getThirdparty();
        $res = $thirdparty->isUserExisted($member->member_data_on_third_party);
        if (!isset($res['status']) || $res['status'] === false) return $this->responseBuilder->fail('Invalid member');
        return $this->responseBuilder->success('Member available', $member);
    }
    
    public function checkMemberDoor(Request $request) {
        $thirdPartyType = $request->input('third_party_type', null);
        $customerId = $request->input('customer_id', null);
        
        $doorId = $request->input('door_id', null);
        $memberId = $request->input('member_id', null);
        $secretKey = $request->input('secret_key', null);
        $door = Door::query()->with('customer')->find($doorId);
        if (!$door || !$door->customer || $door->customer->pente_secret_key !== $secretKey) return $this->responseBuilder->fail('Invalid member');
        $memberDoor = MemberDoor::query()
            ->where('door_id', $doorId)
            ->where('member_id', $memberId)
            ->first();
        if (!$memberDoor) return $this->responseBuilder->fail('Invalid member');
        $member = Member::query()->find($memberDoor->member_id);
        return $this->responseBuilder->success('Member available', $member);
    }
    
    public function dateRegister(Request $request, $memberId) {
        $result = $this->updateDateForMember($request, $memberId);
        if ($result !== false && $result !== true) return $this->responseBuilder->fail($result);
        if ($result === true) return $this->responseBuilder->success('register success');
        else return $this->responseBuilder->fail('register fail, please contact administrator');
    }
    
    public function dateUpdate(Request $request, $memberId) {
        $result = $this->updateDateForMember($request, $memberId);
        if ($result !== false && $result !== true) return $this->responseBuilder->fail($result);
        if ($result === true) return $this->responseBuilder->success('update success');
        else return $this->responseBuilder->fail('update fail, please contact administrator');
    }
    
    public function dateDelete(Request $request, $memberId) {
        $result = $this->updateDateForMember($request, $memberId, true);
        if ($result !== false && $result !== true) return $this->responseBuilder->fail($result);
        if ($result === true) return $this->responseBuilder->success('delete success');
        else return $this->responseBuilder->fail('delete fail, please contact administrator');
    }
    
    protected function store(Request $request) {
        if (!$this->checkMethod('store')) return $this->responseBuilder->fail('Invalid Request', 401);
        $item = new Member();
        $item->fill($request->all());
        if (sizeof($this->defaultModelAttribs) > 0) foreach ($this->defaultModelAttribs as $key => $value) $item->$key = $value;
        //check member_data_on_third_party
        if (is_string($item->member_data_on_third_party) && SystemUtils::isJson($item->member_data_on_third_party))
            $item->member_data_on_third_party = json_decode($item->member_data_on_third_party, true);
        if (!is_array($item->member_data_on_third_party)) return $this->responseBuilder->fail('Field "member_data_on_third_party" is invalid');
        $item->member_data_on_third_party = json_encode($item->member_data_on_third_party);
        //check aws_image_ids
        if ($request->input('aws_image_ids', false) !== false) {
            if (!is_array($item->aws_image_ids)) return $this->responseBuilder->fail('Field "aws_image_ids" must be array');
            $item->aws_image_ids = json_encode($item->aws_image_ids);
        }
        //check member data
        $thirdparty = $this->customer->getThirdparty();
        if ($thirdparty->canAddUser($item->member_data_on_third_party) === false) return $this->responseBuilder->fail('Member not available on third party system');
        $item->password = Hash::make($item->password);
        try {
            if ($item->save()) return $this->responseBuilder->success('Item added', $item);
            else return $this->responseBuilder->fail('Can not add this item');
        } catch (\Exception $e) {
            return $this->responseBuilder->fail($e->getMessage());
        }
    }
    
    protected function update(Request $request, $id) {
        if (!$this->checkMethod('update')) return $this->responseBuilder->fail('Invalid Request', 401);
        $item = Member::query()->where($this->defaultModelAttribs)->find($id);
        if ($item === null) return $this->responseBuilder->fail('Item not found');
        $item->fill($request->all());
        $item->id = $id;
        //check member_data_on_third_party
        if ($request->input('member_data_on_third_party', false) !== false) {
            if (is_string($item->member_data_on_third_party) && SystemUtils::isJson($item->member_data_on_third_party))
                $item->member_data_on_third_party = json_decode($item->member_data_on_third_party, true);
            if (!is_array($item->member_data_on_third_party)) return $this->responseBuilder->fail('Field "member_data_on_third_party" is invalid');
            $item->member_data_on_third_party = json_encode($item->member_data_on_third_party);
        }
        if ($request->input('aws_image_ids', false) !== false) {
            if (!is_array($item->aws_image_ids)) return $this->responseBuilder->fail('Field "aws_image_ids" is invalid');
            $item->aws_image_ids = json_encode($item->aws_image_ids);
        }
        if (sizeof($this->defaultModelAttribs) > 0) foreach ($this->defaultModelAttribs as $key => $value) if (in_array($key, $item->getFillable())) $item->$key = $value;
        if ($item->getAttribute('password') && $request->input('password', false)) $item->password = Hash::make($item->password);
        try {
            if ($item->save()) return $this->responseBuilder->success('Item updated', $item);
            else return $this->responseBuilder->fail('Can not update this item');
        } catch (\Exception $e) {
            return $this->responseBuilder->fail($e->getMessage());
        }
    }
    
    private function updateDateForMember($request, $memberId, $isDelete = false) {
        $member = Member::query()->where('customer_id', '=', $this->customer->id)->where('id', '=', $memberId)->first();
        if (!$member) return 'member not found';
        $startDate = null;
        $endDate = null;
        if (!$isDelete) {
            $startDate = $request->input('start_date', null);
            $endDate = $request->input('end_date', null);
            if (!$startDate || !$endDate) return 'Invalid date';
        }
        $member->start_date = $startDate;
        $member->end_date = $endDate;
        return $member->save();
    }
}
