<?php

namespace App\Http\Controllers\API\Customer;

use App\Model\Door;

class DoorController extends Controller {

    public function __construct() {
        $this->modelClass = Door::class;
        return parent::__construct();
    }

    public function getDoorsWithMemberCount() {
        $this->responseBuilder->data = Door::query()->where('customer_id', '=', $this->customer->id)
            ->leftJoin('member_door', 'door.id', 'member_door.door_id')
            ->selectRaw('door.*, COUNT(member_door.id) AS members')
            ->groupBy('door.id')
            ->paginate();
        return $this->responseBuilder->build(true);
    }
}
