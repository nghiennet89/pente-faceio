<?php

namespace App\Http\Controllers\API\Customer;

use App\Model\Member;
use App\Model\MemberGroup;
use Illuminate\Support\Facades\Route;

class MemberGroupController extends Controller {
    protected $member;
    
    public function __construct() {
        $this->modelClass = MemberGroup::class;
        parent::__construct();
        $memberId = Route::current() ? Route::current()->parameter('memberId') : -1;
        $this->member = Member::query()->where($this->defaultModelAttribs)->find($memberId);
        if (!$this->member) return $this->responseBuilder->fail('Invalid member');
        $this->defaultModelAttribs['member_id'] = $memberId;
    }
}
