<?php

namespace App\Http\Controllers\API\Customer;

use App\Model\Group;
use App\Model\Member;
use App\Model\MemberGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class GroupMemberController extends Controller {
    protected $group;

    public function __construct() {
        $this->modelClass = MemberGroup::class;
        parent::__construct();
        $groupId = Route::current() ? Route::current()->parameter('groupId') : -1;
        $this->group = Group::query()->where($this->defaultModelAttribs)->find($groupId);
        if (!$this->group) return $this->responseBuilder->fail('Invalid member');
        $this->defaultModelAttribs['group_id'] = $groupId;
    }

    protected function index(Request $request) {
        if (!$this->checkMethod('index')) return $this->responseBuilder->fail('Invalid Request', 401);
        $itemPerPage = $request->input('per_page', 200);
        $this->responseBuilder->data = MemberGroup::query()->with('member')->where('group_id', '=', $this->defaultModelAttribs['group_id'])->paginate($itemPerPage);
        return $this->responseBuilder->build(true);
    }

    protected function getAllMemberCanBeAdd() {
        $this->responseBuilder->data = Member::query()
            ->where('customer_id', '=', $this->customer->id)
            ->whereRaw('id NOT IN (select member_id from member_group where group_id = ' . $this->defaultModelAttribs['group_id'] . ')')
            ->get();
        return $this->responseBuilder->build(true);
    }

    protected function deleteMemberGroup($id) {
        $memberGroupId = Route::current() ? Route::current()->parameter('memberG        roupId') : -1;
        if (!$this->checkMethod('destroy')) return $this->responseBuilder->fail('Invalid Request', 401);
        $item = MemberDoor::query()->find($memberDoorId);
        if ($item === null) return $this->responseBuilder->fail('Item not found');
        try {
            if ($item->delete()) return $this->responseBuilder->success('Item deleted');
            return $this->responseBuilder->fail('Can not delete this item');
        } catch (\Exception $e) {
            return $this->responseBuilder->fail($e->getMessage());
        }
    }
}
