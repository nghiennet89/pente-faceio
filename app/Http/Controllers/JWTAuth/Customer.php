<?php

namespace App\Http\Controllers\JWTAuth;

use App\Http\Controllers\Controller;
use App\Utils\ResponseBuilder;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;

class Customer extends Controller {
    use AuthenticatesUsers;

    /*
    |--------------------------------------------------------------------------
    | Login RESBase
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    public function login(Request $request) {
        $token = null;
        $res = new ResponseBuilder();
        try {
            $cre = $this->credentials($request);
            $guard = $this->guard();
            if (!$token = $guard->attempt($cre)) {
                return $res->fail('Auth Fail', 422);
            }
        } catch (JWTException $e) {
            return $res->fail('failed_to_create_token', 500);
        }
        return $res->success('Login success', [
            'token'    => $token,
            'customer' => $guard->user()
        ]);
    }

    protected function guard() {
        return auth('api-customer');
    }

    protected function username() {
        return 'username';
    }
}
