<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DoorAccessLog extends Model {
    
    protected $table    = 'door_access_log';
    protected $fillable = [
        'id',
        'member_id',
        'door_id',
        'created_at',
        'updated_at',
    ];
    
    protected $dates = [
        'created_at',
        'updated_at',
    ];
    
    public function member() { return $this->belongsTo('App\Model\Member', 'member_id', 'id'); }
    
    public function door() { return $this->belongsTo('App\Model\Door', 'member_id', 'id'); }
}
