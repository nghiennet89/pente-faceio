<?php

namespace App\Model;

use App\Utils\BaseThirdpary;
use App\Utils\Nexudus;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Customer extends Authenticatable implements JWTSubject {
    use Notifiable;

    const THIRD_PARTY_AUTH_TYPE = [
        'NONE'    => 0,
        'NEXUDUS' => 1
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'customer';

    protected $fillable = [
        'id',
        'username',
        'email',
        'password',
        'third_party_auth_type',
        'third_party_auth_config',
        'pente_secret_key',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function getJWTIdentifier() {
        return $this->getKey();
    }

    public function getJWTCustomClaims() {
        return [
            'user' => [
                'id' => $this->getKey(),
            ],
        ];
    }

    public function delete() {
        Member::query()->where('customer_id', $this->id)->delete();
        Door::query()->where('customer_id', $this->id)->delete();
        return parent::delete();
    }

    public function getThirdparty() {
        if (!$this->third_party_auth_type || $this->third_party_auth_type === self::THIRD_PARTY_AUTH_TYPE['NONE']) return new BaseThirdpary();
        if ($this->third_party_auth_type === self::THIRD_PARTY_AUTH_TYPE['NEXUDUS']) return new Nexudus($this->third_party_auth_config);
        return null;
    }
}
