<?php

namespace App\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Member extends Authenticatable implements JWTSubject {
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'member';

    protected $fillable = [
        'id',
        'customer_id',
        'username',
        'email',
        'password',
        'member_data_on_third_party',
        'aws_image_ids',
        'start_date',
        'end_date',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'start_date',
        'end_date',
    ];

    public function getJWTIdentifier() {
        return $this->getKey();
    }

    public function getJWTCustomClaims() {
        return [
            'user' => [
                'id' => $this->getKey(),
            ],
        ];
    }

    public function customer() { return $this->belongsTo('App\Model\Customer', 'customer_id', 'id'); }

    public function delete() {
        MemberDoor::query()->where('member_id', $this->id)->delete();
        return parent::delete();
    }
}
