<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Group extends Model {
    
    protected $table    = 'group';
    protected $fillable = [
        'id',
        'customer_id',
        'name',
        'created_at',
        'updated_at',
    ];
    
    protected $dates = [
        'created_at',
        'updated_at',
    ];
    
    public function customer() { return $this->belongsTo('App\Model\Customer', 'customer_id', 'id'); }
}
