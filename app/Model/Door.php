<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Door extends Model {

    protected $table    = 'door';
    protected $fillable = [
        'id',
        'customer_id',
        'ref_id',
        'name',
        'created_at',
        'updated_at',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function customer() { return $this->belongsTo('App\Model\Customer', 'customer_id', 'id'); }

    public function delete() {
        MemberDoor::query()->where('door_id', $this->id)->delete();
        return parent::delete();
    }
}
