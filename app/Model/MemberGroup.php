<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MemberGroup extends Model {
    
    protected $table    = 'member_group';
    protected $fillable = [
        'id',
        'member_id',
        'group_id',
        'created_at',
        'updated_at',
    ];
    
    protected $dates = [
        'created_at',
        'updated_at',
    ];
    
    public function member() { return $this->belongsTo('App\Model\Member', 'member_id', 'id'); }
    
    public function group() { return $this->belongsTo('App\Model\Group', 'member_id', 'id'); }
}
