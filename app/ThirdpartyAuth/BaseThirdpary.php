<?php

namespace App\Utils;

use App\Model\Member;

class BaseThirdpary implements ThirdpartyInterface {

    public function __construct() {

    }

    public function canAddUser($params) {
        if (is_string($params) && SystemUtils::isJson($params)) {
            $params = json_decode($params, true);
        }
        if (is_object($params)) $params = (array)$params;
        if (!is_array($params)) return false;
        $res = $this->isUserExisted($params);
        if ($res['status'] === true) return false;
        else return [
            'status'  => true,
            'message' => 'user available',
            'data'    => [
                'email' => $params['email']
            ]
        ];
    }

    public function isUserExisted($params) {
        $res = [
            'status'  => false,
            'message' => 'invalid user',
            'data'    => null
        ];
        if (SystemUtils::isJson($params)) $params = json_decode($params, true);
        $customer = auth('api-customer')->user();
        if (!$customer) return $res;
        $memberEmail = isset($params['email']) ? $params['email'] : '';
        $member = Member::query()->where('customer_id', $customer->id)
            ->where('email', $memberEmail)
            ->first();
        if (!$member) return $res;
        $res['status'] = true;
        $res['message'] = 'User existed';
        return $res;
    }
}
