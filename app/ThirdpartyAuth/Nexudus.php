<?php

namespace App\Utils;

class Nexudus implements ThirdpartyInterface {

    private $nexudusBasicAuth;

    public function __construct($params) {
        $this->nexudusBasicAuth = isset($params['BasicAuth']) ? $params['BasicAuth'] : null;
    }

    public function canAddUser($params) {
        if(is_string($params) && SystemUtils::isJson($params)) {
            $params = json_decode($params, true);
        }
        if(is_object($params)) $params = (array)$params;
        if(!is_array($params)) return false;
        $res = $this->isUserExisted($params);
        if ($res['status'] === true) return $res;
        else return false;
    }

    public function isUserExisted($params) {
        if (SystemUtils::isJson($params)) $params = json_decode($params, true);
        if (isset($params['Email'])) $params['email'] = $params['Email'];
        $res = [
            'status'  => false,
            'message' => '',
            'data'    => null
        ];
        $userEmail = isset($params['email']) ? $params['email'] : null;
        if (!filter_var($userEmail, FILTER_VALIDATE_EMAIL)) {
            $res['message'] = 'Invalid Email';
            return $res;
        }
        $url = 'https://spaces.nexudus.com/api/sys/users/?User_Email=' . $userEmail;
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_HTTPHEADER     => [
                "authorization: Basic bmdvYy50cmFuQHRvb25nLmNvbS52bjpUb29uZzEyNk1pbmhLaGFp",
                "cache-control: no-cache",
            ],
        ]);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $res['message'] = $err;
            return $res;
        }
        $res['message'] = 'Invalid user';
        if (!SystemUtils::isJson($response)) return $res;
        $response = json_decode($response, true);
        if ($response['TotalItems'] > 0 && isset($response['Records']) && sizeof($response['Records']) > 0) {
            foreach ($response['Records'] as $user) {
                if ($user['Email'] === $userEmail) {
                    $res['status'] = true;
                    $res['message'] = 'User existed';
                    $res['data'] = [
                        'Id'                  => isset($user['Id']) ? $user['Id'] : '',
                        'DefaultBusinessName' => isset($user['DefaultBusinessName']) ? $user['DefaultBusinessName'] : '',
                        'FullName'            => isset($user['FullName']) ? $user['FullName'] : '',
                        'Email'               => isset($user['Email']) ? $user['Email'] : '',
                        'ToStringText'        => isset($user['ToStringText']) ? $user['ToStringText'] : '',
                        'UpdatedBy'           => isset($user['UpdatedBy']) ? $user['UpdatedBy'] : '',
                        'UniqueId'            => isset($user['UniqueId']) ? $user['UniqueId'] : '',
                    ];
                    return $res;
                }
            }
            return $res;
        }
        return $res;
    }
}
