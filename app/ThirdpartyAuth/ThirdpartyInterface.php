<?php

namespace App\Utils;

interface ThirdpartyInterface {
    public function isUserExisted($params);

    public function canAddUser($params);
}
