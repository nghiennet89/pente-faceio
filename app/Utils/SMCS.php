<?php

namespace App\Utils;

class SMCS {

    public static function openDoor($params) {
        $apiBaseUrl = env('SMCS_API_BASE_URL', '');
        $curl = curl_init();
        $data = [
            'door_id'    => isset($params['door_id']) ? $params['door_id'] : null,
            'secret_key' => isset($params['secret_key']) ? $params['secret_key'] : null,
            'action'     => 'ON'
        ];
        curl_setopt_array($curl, array(
            CURLOPT_URL            => $apiBaseUrl . "thirdparty/door-control",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_POSTFIELDS     => json_encode($data),
            CURLOPT_HTTPHEADER     => array(
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return false;
        } else {
            if (!SystemUtils::isJson($response)) return false;
            return json_decode($response, true);
        }
    }
}
