<?php

namespace App\Utils;

class ResponseBuilder {
    const FAIL    = 0;
    const SUCCESS = 1;

    public  $status    = self::FAIL;
    public  $errorCode = null;
    public  $data      = null;
    private $message   = [];

    public function __construct($status = self::FAIL, $errorCode = null, $message = [], $data = null) {
        $this->status = $status;
        $this->errorCode = $errorCode;
        if (is_string($message)) $message = [$message];
        if (!is_array($message)) $message = json_encode($message);
        $this->message = $message;
        $this->data = $data;
    }

    public function build($status = null, $errorCode = null, $message = null, $data = null) {
        if ($status === null) $status = $this->status;
        if ($message === null) $message = $this->message;
        if (is_string($message)) $message = [$message];
        if (!is_array($message)) $message = json_encode($message);
        if ($errorCode === null) $errorCode = $this->errorCode;
        if ($data === null) $data = $this->data;
        $res = [
            'status'  => $status,
            'code'    => $errorCode ? intval($errorCode) : null,
            'message' => $message,
            'data'    => $data,
        ];
        if ($errorCode) return response()->json($res, $errorCode);
        return response()->json($res);
    }

    public function success($message = '', $data = null) {
        $res = [
            'status'  => self::SUCCESS,
            'code'    => null,
            'message' => $message,
            'data'    => $data,
        ];
        return response()->json($res);
    }

    public function fail($message = null, $errorCode = null) {
        $res = [
            'status'  => self::FAIL,
            'code'    => $errorCode,
            'message' => $message,
            'data'    => null,
        ];
        if ($errorCode) return response()->json($res, $errorCode);
        return response()->json($res);
    }

    public function plainText($data = null) {
        if ($data) return response($data, 200)->header('Content-Type', 'text/plain');
        else {
            $res = [
                'status'  => $this->status,
                'code'    => $this->errorCode ? intval($this->errorCode) : null,
                'message' => $this->message,
                'data'    => $this->data,
            ];
            return response(json_encode($res), $res['code'] ? $res['code'] : 200)->header('Content-Type', 'text/plain');
        }
    }
}
