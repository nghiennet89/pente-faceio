<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::post('admin/register', 'AdminController@register');

Route::group(['namespace' => 'JWTAuth'],
    function () {
        Route::group([
            'prefix' => 'admin',
            'as'     => 'admin.'], function () {
            Route::post('login', 'Admin@login');
            Route::post('logout', 'Admin@logout');
        });

        Route::group([
            'prefix' => 'customer',
            'as'     => 'customer.'], function () {
            Route::post('login', 'Customer@login');
            Route::post('logout', 'Customer@logout');
        });

        Route::group([
            'prefix' => 'member',
            'as'     => 'member.'], function () {
            Route::post('login', 'Member@login');
            Route::post('logout', 'Member@logout');
        });
    }
);

Route::group(['namespace' => 'API'],
    function () {
        Route::group([
            'middleware' => 'auth:api-admin',
            'namespace'  => 'Admin',
            'prefix'     => 'admin',
            'as'         => 'admin.'],
            function () {
                Route::resources([
                    'customer'                                     => 'CustomerController',
                    'customer/{customerId}/member'                 => 'MemberController',
                    'customer/{customerId}/member/{memberId}/door' => 'MemberDoorController',
                    'customer/{customerId}/door'                   => 'DoorController',
                    'customer/{customerId}/door/{doorId}/member'   => 'DoorMemberController',
                ]);
            });

        Route::group([
            'middleware' => 'auth:api-customer',
            'namespace'  => 'Customer',
            'prefix'     => 'customer',
            'as'         => 'customer.'],
            function () {
                Route::resources([
                    'member'                 => 'MemberController',
                    'member/{memberId}/door' => 'MemberDoorController',
                    'door'                   => 'DoorController',
                    'door/{doorId}/member'   => 'DoorMemberController',
                ]);
                Route::post('member/{memberId}/date-register', 'MemberController@dateRegister');
                Route::post('member/{memberId}/date-update', 'MemberController@dateUpdate');
                Route::post('member/{memberId}/date-delete', 'MemberController@dateDelete');
                
                Route::post('check-user-on-third-party', 'MemberController@checkUserOnThirdparty');
                Route::post('check-member-on-third-party', 'MemberController@checkMemberOnThirdparty');
                Route::get('door-count-member', 'DoorController@getDoorsWithMemberCount');
                Route::get('door/{doorId}/can-add-member', 'DoorMemberController@getAllMemberCanBeAdd');
                Route::get('info', 'Controller@customerInfo');
                Route::delete('door/member/{memberDoorId}', 'DoorMemberController@deleteMemberDoor');
            });

        Route::group([
            'middleware' => 'auth:api-member',
            'namespace'  => 'Member',
            'prefix'     => 'member',
            'as'         => 'member.'],
            function () {
                Route::resources([
                    'door' => 'DoorController',
                ]);
            });
        Route::post('customer/open-door', 'Customer\Controller@openDoor');
        Route::post('customer/check-member-door', 'Customer\MemberController@checkMemberDoor');
    });
